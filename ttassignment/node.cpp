#include "node.h"
#include <iostream>

SolutionTree::SolutionTree(vector<int> initialMinTerms, int acc) {
    // minimum literal number initially set to infinity
    int size = (int)initialMinTerms.size();
    degree = size - 1;
    accuracy = acc;
    minLiteralNumber = INT_MAX;

    // initialize the root node and push it into the stack
    // NOTE: here size equals to degree plus one, not the highest degree
    int powDegree = (int)pow(2, degree);
    int powAcc = (int)pow(2, acc);
    truthtable att = truthtable(powAcc, vector<char>(powDegree, '0'));
    Node root = Node(att, initialMinTerms);

    NodeStack.push(root);

    // initialize Gray code vector
    rowGrayCode = constructGrayCode(degree);
    colGrayCode = constructGrayCode(acc);
}

void SolutionTree::processStack() {
    while (!NodeStack.empty()) {
        // pop a Node from the stack
        Node currentNode = NodeStack.top();
        NodeStack.pop();

        // process the node
        vector<Node> subNodes = processNode(currentNode);

        // push all branches in the stack
        for (Node nd : subNodes) {
            NodeStack.push(nd);
        }
    }

    assert(minTermEmpty(optimalNode.remainingMinTerms));
    assert(!optimalNode.assignedTruthTable.empty());

    cout << "The Karnaugh map is " << endl;
    for (auto line : optimalNode.assignedTruthTable) {
        for (auto term : line) {
            cout << term;
        }
        cout << endl;
    }

    cout << "The minimum literal number is " << minLiteralNumber << endl;
}

vector<Node> SolutionTree::processNode(Node cNode) {
    vector<Node> ret;

    // cNode stands for current node
    //int size = (int)cNode.remainingMinTerms.size();

    // sum up the number of min-terms
    int numMinTerm = 0;
    for (auto i : cNode.remainingMinTerms) {
        numMinTerm += i;
    }

    // find maximal possible cube(s)
    bool found = false;
    int MTNinCube = (int)floor(log2(numMinTerm)); // log2 min-term number in cube
    while (!found && MTNinCube >= 0) {
        vector<vector<int>> pCubes = possibleCubes(MTNinCube);

        // find if any cubes in pCubes are contained in the remaining
        for (auto c : pCubes) {
            if (containCube(cNode.remainingMinTerms, c)) {
                //found = true;
                // generate sub-nodes and push them in the return value

                // TODO: function to assign cube
                /* === call the function === */
                truthtable newTT = assignTruthTable(cNode.assignedTruthTable, c);

                // if unassignable, go to next cube
                if (newTT[0][0] == 'x') {
                    //found = false;
                    continue;
                }

                // evaluate the truth table, if the number of literal exceeds the 
                // current minimum literal number, then prune
                //if () {
                //    found = false;
                //    continue;
                //}

                // first create a temporary .pla file
                ofstream ofs("temp.pla");
                ofs << ".i " << degree + accuracy << endl;
                ofs << ".o 1" << endl;

                for (unsigned int line = 0; line < newTT.size(); ++line) {
                    for (unsigned int col = 0; col < newTT[line].size(); ++col) {
                        if (newTT[line][col] == '0') continue;
                        ofs << intToBin(line, accuracy - 1) << intToBin(col, degree - 1)
                            << " 1" << endl;
                    }
                }

                ofs << ".e" << endl;

                // TODO: synthesis and get the literal number
                system("mvsis50720.exe -f cmd.rc > tempres.txt");

                ifstream ifs("tempres.txt");

                string tempstring;
                int literalNum;
                while (ifs >> tempstring) {
                    if (tempstring == "lits") {
                        ifs >> tempstring;
                        ifs >> literalNum;
                        break;
                    }
                }

                if (literalNum > minLiteralNumber) continue; // prune this node

                vector<int> newRMT = subtractCube(cNode.remainingMinTerms, c);
                Node newNode(newTT, newRMT);
                found = true;

                // if this is leaf node, update the solution
                if (minTermEmpty(newRMT)) {
                    // update the solution
                    minLiteralNumber = literalNum;
                    optimalNode = newNode;

                    // continue to skip pushing
                    continue;
                }


                /* === push the node into return vector === */
                ret.push_back(newNode);
            }
        }

        --MTNinCube;
    }

    return ret;
}

// logMTN = 2, size = 3, accuracy = 3, return [1, 2, 1], [2, 2, 0], [0, 2, 2], [4, 0, 0] ...
vector<vector<int>> SolutionTree::possibleCubes(int logMTN) {
    vector<vector<int>> ret;

    int powAcc = (int)pow(2, accuracy);

    int lineCube = (degree < logMTN) ? degree : logMTN;

    for (; lineCube >= 0; --lineCube) {
        for (int zeroBefore = 0; zeroBefore <= degree - lineCube; ++zeroBefore) {
            vector<int> line;
            bool toPrune = false;
            for (int i = 0; i < zeroBefore; ++i) {
                line.push_back(0);
            }
            for (int i = 0; i <= lineCube; ++i) {
                long long int tempMTN = choose(lineCube, i) << (logMTN - lineCube);
                //if (tempMTN > powAcc) {
                if (tempMTN >(powAcc * choose(lineCube, i))) {
                    toPrune = true;
                }
                line.push_back(tempMTN);
            }
            for (int i = 0; i < degree - lineCube - zeroBefore; ++i) {
                line.push_back(0);
            }

            if (!toPrune) {
                ret.push_back(line);
            }
        }
    }

    return ret;
}

bool SolutionTree::containCube(vector<int> rmt, vector<int> cube) {
    assert(rmt.size() == cube.size());

    for (unsigned int i = 0; i < rmt.size(); ++i) {
        if (rmt[i] < cube[i]) return false;
    }

    return true;
}


// return the assigned truth table
// if the cube is incapable to assign, return a truth table with the first
// element being 'x'
truthtable SolutionTree::assignTruthTable(truthtable tt, vector<int> cube) {
    // get the line cube and the number of lines
    int multiplier;

    for (auto i : cube) {
        if (i != 0) {
            multiplier = i;
            break;
        }
    }

    vector<int> lineCube; // lineCube divides the cube line by line
    for (auto i : cube) {
        lineCube.push_back(i / multiplier);
    }

    // assign the cube
    // first we need to enumerate all cubes
    // since vector<int> cube can only indecates how many min-terms
    // in each class, not the exact cube
    vector<vector<int>> lineCubeEnum = possibleLineCubeAssignment(lineCube);
    // here the line cubes should be sorted
    // TODO: sort the cubes
    // sort(......)

    auto cubeIt = lineCubeEnum.begin();
    //vector<char> TTline;
    int assignLineNo;

    while (true) {
        // do the assignment
        vector<char> TTline((int)pow(2, degree), '0');
        for (int i : *cubeIt) {
            TTline[i] = '1';
        }

        // if assignable break
        assignLineNo = cubeAssignable(tt, TTline, multiplier);
        if (assignLineNo != -1) {
            break;
        }

        ++cubeIt;

        // if cubeIt exceeds the range, then we cannot assign it
        if (cubeIt == lineCubeEnum.end()) {
            tt[0][0] = 'x';
            return tt;
        }
    }

    for (int lineCount = 0; lineCount < multiplier; ++lineCount) {
        for (int i : *cubeIt) {
            assert((assignLineNo >= 0) && (assignLineNo <= pow(2, accuracy)));
            assert(tt[assignLineNo][i] == '0');
            tt[assignLineNo][i] = '1';
        }
        //tt[assignLineNo] = TTline;

        assignLineNo = nextGrayCode(colGrayCode, assignLineNo);
    }

    return tt;
}

vector<int> SolutionTree::subtractCube(vector<int> rmt, vector<int> cube) {
    assert(containCube(rmt, cube)); // the cube must be in the remaining min-terms

    for (unsigned int i = 0; i < rmt.size(); ++i) {
        rmt[i] -= cube[i];
    }
    return rmt;
}

vector<int> SolutionTree::constructGrayCode(int size) {
    vector<int> GrayCode;
    GrayCode.push_back(0);
    GrayCode.push_back(1);

    for (int i = 1; i < size; ++i) {
        GrayCode = constructGrayCodeHelper(GrayCode);
    }
    return GrayCode;
}

vector<int> SolutionTree::constructGrayCodeHelper(vector<int> gc) {
    vector<int> ret;
    for (unsigned int i = 0; i < gc.size(); ++i) {
        if (i % 2 == 0) {
            ret.push_back(gc[i] << 1);
            ret.push_back((gc[i] << 1) | 1);
        }
        else {
            ret.push_back((gc[i] << 1) | 1);
            ret.push_back(gc[i] << 1);
        }
    }
    return ret;
}

long long int SolutionTree::choose(int n, int k) {

    if ((n < k) || (k < 0)) return 0;

    long long int ret = 1;

    for (int i = 1; i <= k; ++i) {
        ret *= n--;
        ret /= i;
    }

    return ret;
}

vector<int> SolutionTree::listClass(int classNo) {
    assert(classNo <= degree);

    if (classNo == 0) return vector<int>(1, 0);

    vector<int> ret;

    int numOfZero = degree - classNo;

    string str = string(numOfZero, '0') + string(classNo, '1');

    do {
        //int num = 0;
        //for (char c : str) {
        //    num <<= 1;
        //    num |= (int)(c - '0');
        //}
        int num = binToInt(str);

        ret.push_back(num);
    } while (std::next_permutation(str.begin(), str.end()));

    return ret;
}

unordered_set<int> SolutionTree::listClassSet(int classNo) {
    assert(classNo <= degree);

    unordered_set<int> ret;

    if (classNo == 0) {
        ret.insert(0);
        return ret;
    }

    //int powDegree = (int)pow(2, degree);
    int numOfZero = degree - classNo;

    string str = string(numOfZero, '0') + string(classNo, '1');

    do {
        //int num = 0;
        //for (char c : str) {
        //    num <<= 1;
        //    num |= (int)(c - '0');
        //}
        int num = binToInt(str);

        ret.insert(num);
    } while (next_permutation(str.begin(), str.end()));

    return ret;
}

// return the line that the cube is begin to be assigned
// return -1 if it is unassignable
int SolutionTree::cubeAssignable(truthtable tt, vector<char> TTcube, int numOfLine) {
    //for (int i = 0; nextGrayCode(colGrayCode, i) != -1; i = nextGrayCode(colGrayCode, i)) {
    for (int i = 0; i != -1; i = nextGrayCode(colGrayCode, i)) {
        if (!lineCubeAssignable(tt[i], TTcube)) continue;

        bool assignable = true;

        //for (unsigned int j = 0, int k = i; j < numOfLine; ++j, k = nextGrayCode(colGrayCode, k)) {
        int k = i;
        for (int j = 0; j < numOfLine; ++j) {
            //if (nextGrayCode(colGrayCode, k) == -1) return false; // no further checking
            //if (nextGrayCode(colGrayCode, k) == -1) return -1;
            if (k == -1) return -1;

            if (!lineCubeAssignable(tt[k], TTcube)) {
                assignable = false;
                break;
            }

            //if (assignable) return true;
            //if (assignable) return i;

            k = nextGrayCode(colGrayCode, k);
        }

        if (assignable) return i;

        //return false;
        return -1;
    }

    return -1;
    //for (unsigned int i = 0; i < tt.size() - numOfLine; ++i) {
    //    if (!lineCubeAssignable(tt[i], TTcube)) continue;

    //    bool assignable = true;
    //    // assignable
    //    for (unsigned int j = i; j < numOfLine; ++j) {
    //        if (!lineCubeAssignable(tt[j], TTcube)) {
    //            assignable = false;
    //            break;
    //        }
    //    }
    //    if (assignable) return true;
    //}
    //return false;
}

bool SolutionTree::lineCubeAssignable(vector<char> TTline, vector<char> TTcube) {
    assert(TTline.size() == TTcube.size());

    for (unsigned int i = 0; i < TTline.size(); ++i) {
        if ((TTline[i] - '0') & (TTcube[i] - '0')) return false;
    }
    return true;
}

int SolutionTree::nextGrayCode(vector<int> g, int n) {
    for (auto it = g.begin(); it != g.end() - 1; ++it) {
        if (*it == n) return *(it + 1);
    }
    
    return -1;
}

vector<char> SolutionTree::firstEnumOfLineCube(vector<int> cube) {
    vector<char> ret((int)pow(2, degree), '0');

    for (unsigned int i = 0; i < cube.size(); ++i) {
        unordered_set<int> elementsInClassI = listClassSet(i);

        for (int j : rowGrayCode) {
            if (ret[j] == '1') continue; // already assigned

            if (cube[i] == 0) break; // all min-terms in this class have been assigned

            if (elementsInClassI.find(j) != elementsInClassI.end()) {
                // if the current min-term is in the same class

                ret[j] = '1';
                --cube[i];
            }
            //if (elementsInClassI.size() == 0) break;

            //if (elementsInClassI.find(j) != elementsInClassI.end()) {
            //    ret[j] = '1';
            //    elementsInClassI.erase(j);
            //}
        }

        assert(cube[i] == 0);
        //assert(elementsInClassI.empty());
    }
    return ret;
}

vector<vector<int>> SolutionTree::possibleLineCubeAssignment(vector<int> lineCube) {
    unsigned int numOfOne;
    for (numOfOne = 0; numOfOne < lineCube.size(); ++numOfOne) {
        if (lineCube[numOfOne] == 0) continue;
        break;
    }

    unsigned int numOfMinTerm = 0;
    for (unsigned int i = numOfOne; i < lineCube.size(); ++i) {
        numOfMinTerm += lineCube[i];
    }

    unsigned int numOfZero = degree - numOfOne - (int)log2(numOfMinTerm);

    // now we have all infromation

    // next we need to get the basic cube
    vector<string> basicCube = buildInitCube(numOfMinTerm);

    //vector<vector<string>> strRes = insertZero(vector<vector<string>>(1, basicCube), numOfZero);

    //strRes = insertOne(strRes, numOfOne);
    vector<vector<string>> strRes = insertZeroOne(basicCube, numOfZero, numOfOne);

    //// insert zeros first, insert zero intermediate result
    //vector<string> ist0IMResult = zeroOnePermutation(numOfZero, basicCube.size());

    //for (string str : ist0IMResult) {
    //    int pos = 0;
    //    vector<char> temp;
    //    for (unsigned int i = 0; i < str.size(); ++i) {
    //        if (str[i] == 0) {
    //            temp.push_back('0');
    //        }
    //        else {
    //            temp.push_back()
    //        }
    //    }
    //}

    // insert zeros

    // next we will need to convert strings to ints

    vector<vector<int>> ret;
    for (auto temp : strRes) {
        vector<int> tempInt;
        for (auto str : temp) {
            tempInt.push_back(binToInt(str));
        }
        ret.push_back(tempInt);
    }

    return ret;
}

string SolutionTree::intToBin(int num, int highestDegree) {
    assert(num < (int)pow(2, highestDegree + 1));

    string ret;

    while (num) {
        char digit = (char)((num & 1) + '0');
        ret.insert(ret.begin(), 1, digit);

        num >>= 1;
    }

    ret.insert(ret.begin(), highestDegree + 1 - ret.size(), '0');

    return ret;
}

int SolutionTree::binToInt(string str) {
    int ret = 0;
    for (char c : str) {
        ret <<= 1;
        ret |= (int)(c - '0');
    }
    return ret;
}

vector<string> SolutionTree::buildInitCube(int size) {
    vector<string> ret;
    if (size == 1) {
        ret.push_back("");
        return ret;
    }
    int log2Size = (int)log2(size);
    vector<int> GrayCode = constructGrayCode(log2Size);

    for (int i : GrayCode) {
        ret.push_back(intToBin(i, log2Size - 1));
    }

    return ret;
}

vector<string> SolutionTree::starsAndBars(int numOfStar, int numOfBar, char star, char bar) {
    vector<string> ret;
    if (numOfStar == 0) {
        ret.push_back(string(numOfBar, bar));

        return ret;
    }

    if (numOfBar == 0) {
        ret.push_back(string(numOfStar, star));

        return ret;
    }

    for (int left = 0; left <= numOfStar; ++left) {
        int right = numOfStar - left;

        vector<string> subSolution = starsAndBars(left, numOfBar - 1, star, bar);

        for (string partial : subSolution) {
            ret.push_back(partial + bar + string(right, star));
        }
    }

    return ret;
}

vector<string> SolutionTree::zeroOnePermutation(int numOfZero, int numOfOne) {
    string vic = string(numOfZero, '0') + string(numOfOne, '1');
    vector<string> ret;

    do {
        ret.push_back(vic);
    } while (next_permutation(vic.begin(), vic.end()));

    return ret;
}

vector<vector<string>> SolutionTree::insertZero(vector<vector<string>> initialStr, int numOfZero) {
    vector<vector<string>> ret;
    for (vector<string> basicCube : initialStr) {
        // after every loop, we get a intermidiate result vector of cubes
        //vector<vector<string>> IMcubes;

        // get every enumeration
        vector<string> p01 = zeroOnePermutation(numOfZero, basicCube[0].size());

        for (string pattern : p01) {
            vector<string> tempCube;

            for (string str : basicCube) {
                int pos = 0;
                string res;

                for (unsigned int i = 0; i < pattern.size(); ++i) {
                    if (pattern[i] == '0') {
                        res.push_back('0');
                    }
                    else {
                        res.push_back(str[pos++]);
                    }
                }

                tempCube.push_back(res);
            }

            ret.push_back(tempCube);
        }

        //for (auto cube : IMcubes) {
        //    ret.push_back(cube);
        //}
    }
    return ret;
}

vector<vector<string>> SolutionTree::insertOne(vector<vector<string>> initialStr, int numOfOne) {
    vector<vector<string>> ret;
    for (vector<string> basicCube : initialStr) {
        vector<string> p01 = zeroOnePermutation(basicCube[0].size(), numOfOne);

        for (string pattern : p01) {
            vector<string> tempCube;

            for (string str : basicCube) {
                int pos = 0;
                string res;

                for (unsigned int i = 0; i < pattern.size(); ++i) {
                    if (pattern[i] == '1') {
                        res.push_back('1');
                    }
                    else {
                        res.push_back(str[pos++]);
                    }
                }

                tempCube.push_back(res);
            }

            ret.push_back(tempCube);
        }
    }
    return ret;
}

bool SolutionTree::minTermEmpty(vector<int> rmt) {
    for (int i : rmt) {
        if (i != 0) return false;
    }
    return true;
}

vector<vector<string>> SolutionTree::insertZeroOne(vector<string> initialCube, int numOfZero, int numOfOne) {
    vector<vector<string>> ret;

    vector<string> p012 = zeroOneTwoPermutation(numOfZero, initialCube[0].size(), numOfOne);

    for (string pattern : p012) {
        vector<string> tempCube;

        for (string str : initialCube) {
            int pos = 0;
            string res;

            for (unsigned int i = 0; i < pattern.size(); ++i) {
                if (pattern[i] == '0') {
                    res.push_back('0');
                }
                else if (pattern[i] == '2') {
                    res.push_back('1');
                }
                else {
                    res.push_back(str[pos++]);
                }
            }

            tempCube.push_back(res);
        }

        ret.push_back(tempCube);
    }
    
    return ret;
}

vector<string> SolutionTree::zeroOneTwoPermutation(int numOfZero, int numOfOne, int numOfTwo) {
    string vic = string(numOfZero, '0') + string(numOfOne, '1') + string(numOfTwo, '2');
    vector<string> ret;

    do {
        ret.push_back(vic);
    } while (next_permutation(vic.begin(), vic.end()));

    return ret;
}


