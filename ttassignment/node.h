#pragma once
#ifndef NODE_H
#define NODE_H

#define MYDEBUG

#include <vector>
#include <stack>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <string>
#include <unordered_set>
#include <fstream>
#include <unordered_map>

using namespace std;

typedef vector<vector<char>> truthtable;

struct Node {
    Node() {}
    Node(truthtable att, vector<int> rmt) : assignedTruthTable(att), remainingMinTerms(rmt) {}

    truthtable assignedTruthTable;    // truth table with assigned min-terms
    vector<int> remainingMinTerms;              // subject to change after this node processed
};

class SolutionTree {
public:

    // methods
    SolutionTree(vector<int> imt, int acc);     // initial min-terms and accuracy
    void processStack();                        // process the stack

#ifndef MYDEBUG
private:
#endif

    // private methods
    vector<Node> processNode(Node cNode);       // process a single node
    vector<vector<int>> possibleCubes(int logMTN);
                                                // NOTE: size = degree + 1
                                                // logarithm of min-term number
    bool containCube(vector<int> rmt, vector<int> cube);
                                                // rmt: remaining min-terms
    truthtable assignTruthTable(truthtable tt, vector<int> cube);
    vector<int> subtractCube(vector<int> rmt, vector<int> cube);

    // private utilities
    vector<int> constructGrayCode(int size);    // return the required Gray code array
    vector<int> constructGrayCodeHelper(vector<int> gc);   // for generating Gray code
    long long int choose(int n, int k);         // combinatorial number
    vector<int> listClass(int classNo);         // list what are in the class
    unordered_set<int> listClassSet(int classNo);
    int cubeAssignable(truthtable tt, vector<char> TTcube, int numOfLine);
    bool lineCubeAssignable(vector<char> TTline, vector<char> TTcube);
    int nextGrayCode(vector<int> g, int n);     // return the number next to n in Gray code
    vector<char> firstEnumOfLineCube(vector<int> cube);
    vector<vector<int>> possibleLineCubeAssignment(vector<int> lineCube);
    string intToBin(int num, int highestDegree);
    int binToInt(string str);
    vector<string> buildInitCube(int size);
    vector<string> starsAndBars(int numOfStar, int numOfBar, char star = '*', char bar = '|');
    vector<string> zeroOnePermutation(int numOfZero, int numOfOne);
    vector<vector<string>> insertZero(vector<vector<string>> initialStr, int numOfZero);
    vector<vector<string>> insertOne(vector<vector<string>> initialStr, int numOfOne);
    bool minTermEmpty(vector<int> rmt);
    vector<vector<string>> insertZeroOne(vector<string> initialCube, int numOfZero, int numOfOne);
    vector<string> zeroOneTwoPermutation(int numOfZero, int numOfOne, int numOfTwo);

    // data
    int degree;
    int accuracy;

    stack<Node> NodeStack;
    int minLiteralNumber;                       // current minimum literal number
    Node optimalNode;                           // node with the minimum literal number

    vector<int> rowGrayCode;                    // Gray code, size determined by imt
    vector<int> colGrayCode;                    // row is for "n", col is for "m"
};

#endif